#!/usr/bin/env python3

import argparse
from pprint import pformat as pretty

import numpy as np
import matplotlib.pyplot as plt

P = argparse.ArgumentParser()
P.add_argument('H', type=float)

###  V
def v(x, y):
    return np.sin(x) * np.sin(y)

# \frac{dv}{dx}
def dvdx(x, y):
    return np.cos(x) * np.sin(y)

# \frac{dv}{dy}
def dvdy(x, y):
    return np.sin(x) * np.cos(y)

# \frac{d^2 v}{dx^2}
def d2vdx(x, y):
    return -np.sin(x) * np.sin(y)

# \frac{d^2 v}{dy^2}
def d2vdy(x, y):
    return -np.sin(y) * np.sin(x)

# \frac{d^2 v}{dx \cdot dy}
def dvdxy(x, y):
    return np.cos(x) * np.cos(y)

### /V

def gvx(x, y):
    return np.cos(x) * np.sin(y)


def gvy(x, y):
    return np.sin(x) * np.cos(y)


def Wx(dt, x, y):
    # x -> t=0
    # out: t+h
    # w_x(t_k) = \frac{1}{h^2}\cdot (x_{k+1} - 2\cdot x_k + x_{k - 1}) - \frac{dV}{dx}(x_k, y_k)
    return (x[2:] - 2 * x[1:-1] + x[:-2]) / (dt ** 2) - gvx(x[1:-1], y[1:-1])
    # return (x[k + 1] - 2 * x[k] + x[k - 1]) / (h ** 2) - gvx(x[k], y[k])

def Wy(dt, x, y):
    # TODO: correct addition
    # x -> t=0
    # out: t+h
    #
    return (y[2:] - 2 * x[1:-1] + x[:-2]) / (dt ** 2) - gvy(x[1:-1], y[1:-1])
    # return (y[k + 1] - 2 * x[k] + x[k - 1]) / (h ** 2) - gvy(x[k], y[k])


def W(h, x, y):
    return Wx(h, x, y) ** 2 + Wy(h, x, y) ** 2

def L(h, x, y, w):
    return h * w(h, x, y)

def gtaux(h, x, y, wx, wy, d2vx, dvxy):
    # without shift
    wos = wx[1:-1] * (-2/(h ** 2) - d2vx(x[1:-1], y[1:-1]))
    # with shift
    ws = (wx[:-2] + wx[2:]) / (h ** 2)
    # wy part
    yp = wy[1:-1] * dvxy(x[1:-1], y[1:-1])

    return 4 * h * (wos + ws + yp)

def gtauy(h, x, y, wx, wy, d2vy, dvxy):
    # without shift
    wos = wy[1:-1] * (-2/(h ** 2) - d2vy(x[1:-1], y[1:-1]))
    # with shift
    ws = (wy[:-2] + wy[2:]) / (h ** 2)
    # wx part
    xp = wx[1:-1] * dvxy(x[1:-1], y[1:-1])

    return 4 * h * (wos + ws + xp)

def tau(h, x, y, w):
    return np.sum(2 * h * w(h, x[1:-1], y[1:-1]))

# TODO: alpha???

def mep(params):
    h = params['h']
    x = params['x']
    y = params['y']

    i = 1

    while True:
        r = (x, y)
        #w = wk(h, x, y)
        wx = Wx(x, y)
        wy = Wy(x, y)
        l = L(h, w)
        print('i = %d, L = %s' % (i, pretty(l)))
        i += 1
        tau = gtaux(h, x, y, d2dvx, d2dvy, dvdxy)


X0 = Y0 = 3 * np.pi / 2
X1 = Y1 = 5 * np.pi / 2

if __name__ == '__main__':
    DOTS = 5
    x0 = np.linspace(X0, X1, DOTS)
    y0 = np.linspace(Y0, Y1, DOTS)

    dx = np.zeros(x0.shape)
    dy = np.zeros(y0.shape)

    EPS = 10 ** (-8)
    DT = 1

    dx[1:-1] = EPS * np.random.randn()
    dy[1:-1] = EPS * np.random.randn()
    print('dx = %s' % pretty(dx))
    print('dy = %s' % pretty(dy))

    x1 = x0 + dx
    y1 = y0 + dy

    tau0 = tau(DT, x0, y0, W)
    tau1 = tau(DT, x1, y1, W)
    print('tau0 = %f, tau1=%f' % (tau0, tau1))

    wx1 = Wx(DT, x1, y1)
    wy1 = Wy(DT, x1, y1)
    gtau1x = np.zeros(DOTS)
    gtau1y = np.zeros(DOTS)
    gtau1x[1:-1] = gtaux(DT, x1, y1, wx1, wy1, d2vdx, dvdxy)
    gtau1y [1:-1]= gtauy(DT, x1, y1, wx1, wy1, d2vdy, dvdxy)
    print('gtau1x =', pretty(gtau1x))
    print('gtau1y =', pretty(gtau1y))

    foo = np.dot(gtau1x, dx) + np.dot(gtau1y, dy)
    print(foo)



# if __name__ == '__main__':
#     args = P.parse_args()
#
#     p = {'h': args.H,
#          'x': np.array([]),
#          'y': np.array([])}
#
#     mep(p)
